# Assistance vidéo à l'arbitrage

*UTILS_google_alertes,Newsletter_futura-science,La Presse,Inoreader*

***24/09/2021:*** 
# High-Tech : les nouvelles technologies du football moderne
https://www.tests-et-bons-plans.fr/2018/07/high-tech-les-nouvelles-technologies-au-service-du-football-moderne.html
- Les arbitres vidéo sont en dehors du stade dans un véhicule ou préfabriqué spécifiquement conçu et disposent d'écrans et du contrôle des images et ralentis qui leur permettent d'indiquer à l'arbitre par le biais de son oreillette le résultat du visionnage.
- 4 caméras HD/3G SDI capables de filmer en 1080i et en 1080p
- Une fourgonnette OB qui génère une division quadruple
- Un signal SDI représentant l’interface MP0
- Moniteur VAR avec lecture quadruple
- Un signal SDI représentant l’interface MP1
- Un convertisseur vidéo analogique HD/3G SDI
- Un extracteur de synchronisation de trame analogique
- Deux ensembles d’équipement d’acquisition SDI HD/3G
- Deux ordinateurs portables pour enregistrer les signaux SDI
- Un stroboscope
- Le stroboscope doit être réglable en intensité et en retard entre le signal de trig à la
stroboscope et le flash. 


***27/09/2021:***

https://www.cnetfrance.fr/news/euro-2021-comment-marche-la-var-et-comment-va-t-elle-s-ameliorer-39923229.htm
- Les flux vidéo sont synchronisés et envoyés en direct (sans latence) à la “VAR room” et aux AAV par fibre optique. Ils peuvent être enregistrés par le fournisseur de la technologie du système, ou par l'organisateur de la compétition.
La prochaine étape, c’est le développement d’une VAR “automatisée”, ou presque, grâce à l’IA. La FIFA teste depuis 2019 une technologie de suivi des joueurs. Ceux-ci sont détectés automatiquement, puis modélisés en temps réel sous la forme de “squelettes” sur les images du match, en combinaison des lignes du terrain virtuelles recréées par la VOL.


***27/09/2021:***
# Le VAR va disposer d'une ligne de hors-jeu 3D après la trêve internationale
https://www.dhnet.be/sports/football/division-1a/le-var-va-disposer-d-une-ligne-de-hors-jeu-3d-a-partir-de-la-11e-journee-mi-octobre-61519519d8ad581e9889d7da(via google alert)
-  l'assistance d'arbitrage video devrait bien disposer d'une ligne de hors-jeu 3D à partir de la 11e journée de championnat mi-octobre. Pour l'heure, le VAR ne pouvait utiliser qu'une ligne de hors-jeu 2D.
Le porte-parole de la Pro League a confirmé que le système devrait être d'application après la trêve internationale mi-octobre.


***20/10/2021:***
# Brevet insolite : L'assistance vidéo à l'arbitrage, une innovation de pointe
https://yesmypatent.com/blog/4/post/brevet-insolite-l-assistance-video-a-l-arbitrage-une-innovation-de-pointe-29 (via google)
- Sony dépose le 29 juin 2015 un brevet contenant un dispositif et une méthode de traitement d’image pour commander automatiquement le mouvement d’une caméra pour suivre un objet et prédire la future position de celui-ci, et ainsi contrôler le champ de vision pour que la caméra enregistre la scène à la position future
Aujourd’hui contemporains d’arbitres augmentés grâce à la technologie vidéo, demain le serons-nous d’arbitres robotisés ou simplement d’intelligence artificielle veillant au bon déroulement d’un match de foot ? L’avenir nous le dira.


***20/10/2021:***
# comment marche la VAR et comment va-t-elle s'améliorer ?
https://www.cnetfrance.fr/news/euro-2021-comment-marche-la-var-et-comment-va-t-elle-s-ameliorer-39923229.htm
- La prochaine étape, c’est le développement d’une VAR “automatisée”, ou presque, grâce à l’IA. La FIFA teste depuis 2019 une technologie de suivi des joueurs. Ceux-ci sont détectés automatiquement, puis modélisés en temps réel sous la forme de “squelettes” sur les images du match, en combinaison des lignes du terrain virtuelles recréées par la VOL.

- D’ici 2022, donc dès la Coupe du Monde au Qatar, un tel système devrait permettre d’identifier d’une façon quasi instantanée les hors-jeu, et de réaliser des appels de hors-jeu “semi-automatisés”. L’arbitre serait alerté en quelques secondes, et pourrait ainsi prendre une décision beaucoup plus vite. Les assistants n'auraient plus à attendre pour lever leur drapeau, et les supporters pourraient célébrer les buts sans craindre qu'un hors-jeu soit repéré.

- Bien que Hawk-Eye développe apparemment son propre logiciel, c’est surtout ChyronHego qui planche sur cette “VAR 2.0” pour la FIFA. L’entreprise suédoise propose depuis déjà 2017 une technologie de GLT ; un système de suivi optique des objets en mouvements, TRACAB, qui suit le ballon et les joueurs sur le terrain. Mais elle reste perfectible. L’étalonnage des caméras autour du terrain est souvent perturbé par les grillages positionnés entre la caméra et le terrain. Et plus globalement, il reste possible que le moment où le ballon est frappé (ou la passe effectuée) se situe entre deux images, et échappe au système.


***22/10/2021:***
# L’ouverture des micros pour plus de pédagogie 
https://www.francetvinfo.fr/sports/foot/arbitrage-video-la-sonorisation-comme-terrain-d-entente-dans-le-football_4807645.html (via google alerts)
- “Ouvrir la sonorisation permettrait aux supporters de mieux comprendre la VAR, et donc d’éduquer le public à l’arbitrage vidéo”, Gaël Angoula, ancien joueur professionnel, et arbitre en Ligue 2.

-  Une refonte du modèle qui permettrait donc à la fois de réduire les contestations des supporters et les sentiments d’injustice de certains joueurs ou entraîneurs, mais aussi d’améliorer le respect et les comportements de tous les acteurs du jeu sur le terrain.

***29/10/2021***
# Coupe du Monde 2022 : Vers l’utilisation des arbitres de touche robots
https://africafootunited.com/coupe-du-monde-2022-vers-lutilisation-des-arbitres-de-touche-robots/
- La FIFA envisage d’utiliser une nouvelle technologie lors de la Coupe du Monde 2022 au Qatar. Il s’agit du robo-lineman qui est spécialisé pour détecter les hors-jeu au cours d’un match. Un total de 29 points du corps de chaque joueur sont suivis par l’algorithme pour fournir des informations sur les positions des joueurs. Cette nouvelle technologie est beaucoup plus précise que les caméras VAR.

- Une réunion de l’International FA Board devrait être faite avec plusieurs experts dont Arsène Wenger en vue de juger de la capacité de ce robot à faire le travail. A l’issue de cette réunion, la FIFA sera fixée sur le fait que cette technologie pourrait être utilisée ou non dans le cadre de la plus grande compétition de football au monde.

***14/11/2021***
# Sonorisation, VAR sur les écrans des stades: les arbitres français veulent plus de transparence
https://rmcsport.bfmtv.com/football/ligue-1/sonorisation-var-sur-les-ecrans-des-stades-les-arbitres-francais-veulent-plus-de-transparence_AV-202111110231.html
- Le SAFE désire humaniser l’arbitrage, et permettre aux téléspectateurs et aux observateurs de mieux comprendre les décisions en temps réel. Le premier point important est la sonorisation des arbitres, autrement dit le fait de rendre public les échanges entre les arbitres de terrain et avec l’assistance vidéo, mais également entre l’arbitre et les joueurs.

***30/11/2021***
# Coupe du monde 2022 : les hors-jeu pourraient être détectés automatiquement par une IA
https://www.01net.com/actualites/coupe-du-monde-2022-les-hors-jeu-pourraient-etre-detectes-automatiquement-par-une-ia-2051770.html
- Selon les informations du média anglais The Times, la technologie mise en place par Hawk-eye traque le corps de chaque joueur sur 29 points. Grâce à cette modélisation squelettique, elle a la possibilité d'identifier le bout du corps qui serait éventuellement hors-jeu en une demi-seconde.
L'IA serait aussi capable de détecter le moment précis où le ballon a été touché, afin de prendre une décision éclairée. Elle transmet instantanément son observation à l'arbitre en charge de la vidéo qui, une fois qu'il l'a validée, donne son verdict à l'arbitre central. 

***30/11/2021***
# Foot: la détection "semi-automatisée" du hors jeu va être testée en compétition officielle
https://www.varmatin.com/football/foot-la-detection-semi-automatisee-du-hors-jeu-va-etre-testee-en-competition-officielle-730331
- Il s'agit de disposer "10 à 12 caméras" sous le toit de chaque stade, pour suivre les joueurs et aider les arbitres à apprécier deux points cruciaux: le moment où le ballon est passé ou touché, et la position de chaque partie du corps des joueurs impliqués par rapport à la ligne imaginaire de hors-jeu.
- Les données collectées seront transmises quasiment en temps réel à la cellule d'assistance vidéo à l'arbitrage (VAR), la décision finale revenant toujours à l'arbitre lui-même, rappelle l'instance du football.

***6/12/2021***
# L’AFC teste l’assistance vidéo simplifiée en Coupe des clubs de football féminin 
https://www.fifa.com/fr/news/lafc-teste-lassistance-video-simplifiee-en-coupe-des-clubs-de-football
- La Confédération Asiatique de Football (AFC) a testé une version simplifiée de l’assistance vidéo à l’arbitrage à l’occasion de l’édition pilote de la Coupe des clubs de football féminin de l’AFC 2021 (ouest), organisée en Jordanie du 7 au 13 novembre derniers. 
- L’AFC a testé ce nouveau concept dans le cadre de sa démarche d’intégration des avancées technologiques dans le sport. Elle entend ainsi contribuer à la création de systèmes d’assistance vidéo à l’arbitrage plus accessibles, auxquels des compétitions au budget plus limité pourraient avoir recours.

***22/01/22***
# Football. La VAR light, une nouvelle révolution dans l’arbitrage vidéo ?
https://www.lobservateur.fr/sports/sports-avesnois/2022/01/19/lentente-feignies-aulnoye-developpe-et-mise-sur-loutil-video/
-  En haut des tribunes, derrière un ordinateur, un arbitre vidéo oscille entre son écran et le match qu’il observe de ses propres yeux. Sur les quelques occasions chaudes de la rencontre 
- Depuis 2019, l’entreprise Dartfish élabore un système de VAR light, tout comme ses concurrents Vogo et Hawk-Eye. L’objectif ? Proposer d’ici deux ans une solution d’assistance vidéo à l’arbitrage moins chère et plus rapide, pour la Ligue 2, les championnats fédéraux français et les premières divisions de pays en voie de développement africains ou sud-américains. 

***4/02/22***
# Innovation technologique : La FIFA veut tester un « arbitre robot »
https://www.burkina24.com/2022/02/03/innovation-technologique-la-fifa-veut-tester-un-arbitre-robot/
- La Fédération internationale de football association (FIFA)  annonce que de nouvelles technologies d’arbitrage en guise d’assistance à l’arbitre central seront testées pour la toute première fois lors de la Coupe du Monde des Clubs qui débute ce jeudi 3 février 2022 aux Émirats Arabes Unis.

« Arbitre robot » suivi du mouvement du ballon et des joueurs à l’aide de 12 caméras installées. 10 à 12 caméras disposées sous le toit du stade suivent les joueurs et le ballon. Le système analyse la position de tous les joueurs impliqués dans l’action. Les données sont transmises en temps réel à l’assistance vidéo de l’arbitrage, et la décision finale revient à l’arbitre.

- En cas de succès, la FIFA émet même la possibilité d’utiliser ce système d’arbitrage lors de la Coupe du Monde 2022 au Qatar. Pour information, cette coupe organisée par la Fédération internationale de football (FIFA) se tiendra cette année aux Emirats arabes unis.

***13/02/22***
# FIFA : Un VAR à coût réduit en étude
https://lefootenbref.com/2022/02/12/fifa-un-var-a-cout-reduit-en-etude/
- Démocratisée depuis plus de cinq ans, l’assistance vidéo à l’arbitrage est maintenant utilisée dans la plupart des championnats et des compétitions. Cependant, cette révolution technologique a un coût très élevé. En Ligue 1, la société britannique Hawk-Eye facturerait son service à hauteur de six millions d’euros par saison.

- En ce sens, la FIFA travaillerait actuellement sur une version à coût réduit du VAR, notamment pour les divisions inférieures. Contacté par le journal L’Équipe, Adrien Baquet, manager de la société suisse Dartfish, aurait trouvé « un système dix fois moins cher avec un niveau de qualité restant élevé (…) on peut très bien utiliser le VAR sans diffuseur », explique-t-il. La FIFA supervise, notamment en France, des expérimentations visant à démocratiser l’assistance vidéo à l’arbitrage. Un dispositif a déjà été essayé lors du match de National 1, Créteil-Le Mans, en janvier dernier.


