<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link
      href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900"
      rel="stylesheet"
    />

    <title>Reflux HTML CSS Template</title>
<!--
Reflux Template
https://templatemo.com/tm-531-reflux
-->
    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Additional CSS Files -->
    <link rel="stylesheet" href="assets/css/fontawesome.css" />
    <link rel="stylesheet" href="assets/css/templatemo-style.css" />
    <link rel="stylesheet" href="assets/css/owl.css" />
    <link rel="stylesheet" href="assets/css/lightbox.css" />
    <?php
  $page_name = "index.php";
?>
<title><?php echo $page_name; ?></title>
  </head>
  <?php
  include "compteur.php";
  $nombre_visiteurs = visiteur($page_name);
  ?>
 
  <body>
    <div id="page-wraper">
      <!-- Sidebar Menu -->
      <div class="responsive-nav">
        <i class="fa fa-bars" id="menu-toggle"></i>
        <div id="menu" class="menu">
          <i class="fa fa-times" id="menu-close"></i>
          <div class="container">
            <div class="image">
              <a href="#"><img src="assets/images/pdp.jpg" alt="" /></a>
            </div>
            <div class="author-content">
              <h4>Erambert Quentin</h4>
              <span>Etudiant en deuxieme années de BTS</span>
            </div>
            <nav class="main-nav" role="navigation">
              <ul class="main-menu">
                <li><a href="#section1">Qui Suis Je ?</a></li>
                <li><a href="#section2">Compétences</a></li>
                <li><a href="#section4">Mes experiences</a></li>
                <li><a href="#section3">Mes travaux</a></li>
                <li><a href="#section6">Veille technologique</a></li>
                <li><a href="#section5">Me retrouver</a></li>
                <li><a href="#section7">Avis</a></li>
              </ul>
            </nav>
            <div class="social-network">
              <ul class="soial-icons">
                <li>
                  <a href="https://gitlab.com/erambert.quentin/ERAMBERTs"
                    ><i class="fa fa-git"></i
                  ></a>
                </li>
                <li>
                  <a href="https://www.linkedin.com/feed/?trk=homepage-basic_google-one-tap-submit"><i class="fa fa-linkedin"></i></a>
                </li>
                <li>
                  <a href="https://my.indeed.com/p/quentine-dejx1fo"><i class="fa fa-Indeed"></i></a>
                </li>
                
              </ul>
            </div>
            <div class="copyright-text">
              <p>Copyright 2019 Reflux Design</p>
            </div>
          </div>
        </div>
      </div>

      <section class="section about-me" data-section="section1">
        <div class="container">
          <div class="section-heading">
            <h2>Qui Suis Je ?</h2>
            <div class="line-dec"></div>
            <span
              >Je m'appelle quentin erambert je suis acteuellement un eleve de deuxime années de BTS SIO j'ai etudié dans un bas STI2D option SIN</span
            >
          </div>
          <div class="left-image-post">
            <div class="row">
              <div class="col-md-6">
                <div class="left-image">
                  <img src="assets/images/linkedin-petit.png" height="220" width="70" />
                </div>
              </div>
              <div class="col-md-6">
                <!-- <div class="right-text"> -->
                  <h4>Centre d'interets</h4>
                  <p>
                    Club EACPA (Cergy) - Athlétisme.  (100 m – Saut en longueur) - Ce sport m’a permis de gérer la pression et m’a appris la rigueur
                  </p>
                  <div class="white-button">
                    <a href="https://www.linkedin.com/in/quentin-erambert-244b40207/">linkedin</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="right-image-post">
            <div class="row">
              <div class="col-md-6">
                <div class="left-text">
                  
                </div>
              </div>
              <div class="col-md-6">
                <div class="right-image">
                  <img src=""  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section class="section my-services" data-section="section2">
        <div class="container">
          <div class="section-heading">
            <h2>Compétences</h2>
            <div class="line-dec"></div>
            <span
              ></span
            >
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="service-item">
                <div class="second-service-icon service-icon"></div>
                <h4>HTML &amp; CSS</h4>
                <p>
                  langage informatique qui décrit la présentation des documents HTML et XML.J'ai retenue le projet ci-dessous lors duquel j'ai utilisé du CSS et l'HTML.
                </p>
                <div class="white-button">
                  <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Otelo/public/css">Aller vers projets</a>
                </div>
              </div>
            </div>
            
            <div class="col-md-6">
              <div class="service-item">
                <div class="second-service-icon service-icon"></div>
                <h4>JavaScript</h4>
                <p>
                  JavaScript est un langage de programmation de scripts principalement employé dans les pages web interactives
                  exemple de projet ci-dessous
                </p>
                <div class="white-button">
                  <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/blob/master/Finder/Finder_00/index.js">Aller vers projets</a>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="service-item">
                <div class="
                second-service-icon service-icon"></div>
                <h4>PHP</h4>
                <p>
                  Au cours de ces deux années j'ai pu a travers mes projets approfondir mes connaissances en php
                </p>
                <div class="white-button">
                  <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Finder/Finder_00">Aller vers projets</a>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="service-item">
                <div class="second-service-icon service-icon"></div>
                <h4>Python</h4>
                <p>
                  langage etudier en début de 1ere année de BTS
                </p>
                <div class="white-button">
                  <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/B1_Python/Fish1">Aller vers projets</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section class="section my-work" data-section="section4">
        <div class="container">
          <div class="section-heading">
            <h2>Mes experiences</h2>
            <div class="line-dec"></div>
            <span
              >Mai à Juillet 2021 - Stage de 6 semaines dans le service informatique de BNP Paribas Paris (Valmy 2) – Montreuil (93)
              pratique de Laravel, mise en place d’un socle, gestion de vue et de controller.</span
            >
            <div class="white-button">
              <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/blob/master/AP/Stage.md">En savoir plus</a>
            </div>
            <div class="line-dec"></div>
            <span
            >janvier 2022 a fevrier 2022 - Stage dans un centre socio culturel qui consistait a mettre en place un formulaire d'inscription.</span
          >
          <div class="white-button">
            <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/blob/master/AP/Stage.md">En savoir plus</a>
          </div>


      <section class="section my-work" data-section="section3">
        <div class="container">
          <div class="section-heading">
            <h2>Mes travaux</h2>
            <div class="line-dec"></div>
            <span
              ></span
            >
          </div>
          <div class="row">
            <div class="isotope-wrapper">
              <form class="isotope-toolbar">
                <label
                  ><input
                    type="radio"
                    data-type="*"
                    checked=""
                    name="isotope-filter"
                  />
                  <span>Tous</span></label
                >
                
              </form>
              <div class="isotope-box">
                <div class="isotope-item" data-type="nature">
                  <figure class="snip1321">
                    <img
                      src="assets\images\easy-line-200x141.png"
                      alt=""
                    />
                    <figcaption>
                      <a
                        href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/easyline"
                        
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Easyline</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="people">
                  <figure class="snip1321">
                    <img
                      src="assets\images\Fichier-1-200x251.png"
                      alt="Finder"
                    />
                    <figcaption>
                      <a
                        href="Finder.html"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Finder</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="animals">
                  <figure class="snip1321">
                    <img
                      src="assets\images\Fichier-1-200x200.png"
                    />
                    <figcaption>
                      <a
                        href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Oless/Oless_1/MyApplication2"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Oless</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="people">
                  <figure class="snip1321">
                    <img
                      src="assets/images/Otelo.png"
                    />
                    <figcaption>
                      <a
                        href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Otelo"
                        
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Otelo</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="nature">
                  <figure class="snip1321">
                    <img
                      src="assets/images/chef-200x200.jpg"
                    />
                    <figcaption>
                      <a
                        href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Ristoo"
                      
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Ristoo</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>

                <div class="isotope-item" data-type="animals">
                  <figure class="snip1321">
                    <img
                      src="assets/images/frebourg-ninja-world_of_warcraft_logo-200x79.png"                
                    />
                    <figcaption>
                      <a
                        href="https://gitlab.com/erambert.quentin/ERAMBERT/-/tree/master/Warcraft/mission1"
                        ><i class="fa fa-search"></i
                      ></a>
                      <h4>Warcraft</h4>
                      <span></span>
                    </figcaption>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    
            <section class="section my-work" data-section="section6">
              <div class="container">
                <div class="section-heading">
                  <h2>Veille technologique</h2>
                  <span>comment marche la VAR et comment va-t-elle s'améliorer ?</span>
                  <div class="line-dec"></div>
                  <form>
                    <div class="white-button">
                      <a href="https://gitlab.com/erambert.quentin/ERAMBERT/-/blob/master/Veille/Veille_technologique.md">en savoir plus</a>
                    </div>
                </form>                  
            
                  
                </div>

      <section class="section contact-me" data-section="section5">
        <div class="container">
          <div class="section-heading">
            <h2>Me retrouver</h2>
            <div class="line-dec"></div>
            <div class="white-button">
              <a href="https://www.linkedin.com/in/quentin-erambert-244b40207/">linkedin</a>
            </div>
            <div class="line-dec"></div>
            <div class="white-button">
              <a href="https://gitlab.com/erambert.quentin/ERAMBERT">gitlab</a>
            </div>
            <div class="line-dec"></div>
            <div class="white-button">
              <a href="https://my.indeed.com/p/quentine-dejx1fo">indeed</a>

            </div>
            
                    </div>
                  </div>
                  <section  >
                    <div class="container">
                      <div class="section-heading">
                        <h2>Avis</h2>
                        <div class="line-dec"></div>
<form  method="post" action="pagederemerciements.php" class="form"  >
                  <p>
                    <label for="nom">Nom </label>
                    <input type="text" name="nom" id="nom" />
                  </p>
                  
                  <p>
                    <label for="prenom">Prénom </label>
                    <input type="text" name="prenom" id="prenom" required/>
                  </p>
                  
                  <p>
                    <label for="email">Email </label>
                    <input type="email" name="email" id="email" required/>
                  </p>
                  
                  <p>
                    <label for="message">Message </label><br />
                    <textarea type="text" name="avis" cols="45" rows="10" required></textarea>
                  </p>
                  
                  <p>
                    <input type="submit" value="Envoyer" />
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>

    <!-- Scripts -->
    <!-- Bootstrap core JavaScript -->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <script src="assets/js/isotope.min.js"></script>
    <script src="assets/js/owl-carousel.js"></script>
    <script src="assets/js/lightbox.js"></script>
    <script src="assets/js/custom.js"></script>
    <script>
      //according to loftblog tut
      $(".main-menu li:first").addClass("active");

      var showSection = function showSection(section, isAnimate) {
        var direction = section.replace(/#/, ""),
          reqSection = $(".section").filter(
            '[data-section="' + direction + '"]'
          ),
          reqSectionPos = reqSection.offset().top - 0;

        if (isAnimate) {
          $("body, html").animate(
            {
              scrollTop: reqSectionPos
            },
            800
          );
        } else {
          $("body, html").scrollTop(reqSectionPos);
        }
      };

      var checkSection = function checkSection() {
        $(".section").each(function() {
          var $this = $(this),
            topEdge = $this.offset().top - 80,
            bottomEdge = topEdge + $this.height(),
            wScroll = $(window).scrollTop();
          if (topEdge < wScroll && bottomEdge > wScroll) {
            var currentId = $this.data("section"),
              reqLink = $("a").filter("[href*=\\#" + currentId + "]");
            reqLink
              .closest("li")
              .addClass("active")
              .siblings()
              .removeClass("active");
          }
        });
      };

      $(".main-menu").on("click", "a", function(e) {
        e.preventDefault();
        showSection($(this).attr("href"), true);
      });

      $(window).scroll(function() {
        checkSection();
      });
    </script>
  </body>
</html>
