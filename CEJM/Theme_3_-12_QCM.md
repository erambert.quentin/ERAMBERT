Thème 3 : L’organisation de l’activité de l’entreprise
Chapitre XII – La gestion des risques liés à l’activité de l’entreprise
Remarque : plusieurs réponses sont possibles
DOCUMENT 1 p170

Q1 : L’incendie à l’usine a été causé par :

□ Une étincelle électrique sur l’installation

[x] La réparation d’une cuve de lubrifiant

□ La cigarette encore allumée du plombier

Q2 : Le progrès technique et les accidents en entreprise sont-ils liés ?
Oui car :

[x] Le progrès technique entraîne des accidents

[x] la pollution est dangereuse pour l’homme

[x] les machines peuvent entraîner des explosions ou des dysfonctionnements

Non car :

□ Les matières transformées (métaux en fusion ou produits corrosifs) n’affectent pas
les salariés

□ Les véhicules automobiles sont sûrs et ne provoquent pas d’accidents sur les trajets

□ Les salariés sont éloignés des machines

Q3 : Qui a été touché par cet incendie ?

□ Le dirigeant

□ Les pompiers

□ Le DRH

[x] Le plombier

[x] Les salariés

□ Le directeur de la production

Q4 : De manière plus générale, qui sont les victimes de ces accidents ? Vous les relierez aux raisons.

[x] Les actionnaires

[x] Baisse d’activité

[x] Les clients

[x] Pollution

[x] Les citoyens

□ Retard de livraison

□ Les salariés

□ Blessures

□ Les fournisseurs

□ Chômage

□ L’entreprise

DOCUMENT 2 p170

Q5 : Comment s’est traduit le piratage informatique ?

□ Le vol d’une clé

[x] Le chef d’entreprise a été racketté

[x] Les données de l’ordinateur ne sont plus accessibles

□ Les tâches administratives peuvent être exécutées

[x] L’activité de l’entreprise est interrompue

□ Le chef d’entreprise a payé une rançon

Q6 : Pourquoi les cyberattaques coûtent chères aux entreprises ?

□ Elles paient la rançon

□ Les salariés sont payés double

□ Les cyberattaques entraînent des pertes d’activité

□ L’entreprise doit racheter des PC neufs

□ L’entreprise doit mettre en place des procédures de substitution

□ L’entreprise doit déménager

□ L’entreprise doit faire appel à des spécialistes techniques pour résoudre les problèmes

DOCUMENT 3 p170

Q7 : Quels sont les risques physiques des salariés ?

[x] Les manutentions manuelles

□ Les déplacements au sein de l’entreprise

□ Les chuts des salariés

□ Les blessures résultant de l’utilisation de divers outillages

□ Les accidents lors des trajets des salariés pour se rendre au travail

Q8 : Pourquoi ces risques impactent la vie de l’entreprise ?

□ Ils donnent une mauvaise image de l’entreprise

□ Ils entraînent des absences des salariés

□ Ils entraînent une baisse de l’activité

□ Ils entraînent des coûts supplémentaires pour protéger les salariés

□ Ils entraînent une baisse des accidents du travail

□ Ils entraînent une augmentation des cotisations sociales (accident du travail)

DOCUMENT 4 p171

Q9 : Quelles sont les contraintes imposées à l’employeur du fait de son obligation de sécurité ?

[x] L’employeur doit veiller à la santé de son personnel

[x] L’employeur doit respecter les règles d’aménagement des locaux

[x] L’employeur doit évaluer les risques professionnels des postes de travail

[x] L’employeur doit veiller à la sécurité au travail de ses salariés

Q10 : Les risques pour la santé des salariés sont-ils plus faciles à prévenir que les risques courus par
l’entreprise ?

□ Non, car il est plus facile d’identifier des actes de malveillance, par exemple

□ Oui car les risques liés à la santé des salariés sont plus faciles à identifier

□ Les deux types de risques sont aussi difficiles à prévenir l’un que l’autre

DOCUMENT 5 p171

Q11 : Que sont les risques psychosociaux ?

□ Ce sont des symptômes qui affectent la psychologie et la santé mentale des salariés

□ Ce sont des symptômes qui empêchent le salarié de s’intégrer dans l’entreprise

Q12 : Comment se manifestent les troubles 
psychosociaux ?

□ Angoisse

□ Fatigue

□ Bonne humeur

□ Stress

□ Dépression

□ Joie de vivre

Q13 : Les risques psychosociaux ont un impact sur

□ La santé mentale des salariés

□ Le fonctionnement de l’entreprise

□ L’efficience de l’entreprise

□ La satisfaction des clients

□ La santé physique des salariés
