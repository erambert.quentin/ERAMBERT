# QUENTIN ERAMBERT

# Question 1
Gestionnaire Libre de Parc Informatique(*source* **wikipedia**)

# Question 2
GLPI est un logiciel libre de gestion des services informatiques et de gestion des services d'assistance. Cette solution libre est éditée en PHP et distribuée sous licence GPL.(*source* **wikipedia**)


# Question 3
1. Parc
2. assistance
3. configuration
4. outils 
5. administarion
6. gestion
(source GLPI.org)    
    

# Question 4
- Le module Parc permet d'accéder aux différents matériels inventoriés.
- Le module Assistance permet de créer, suivre les tickets, et voir les statistiques.
- Le module Gestion permet de gérer les contacts, fournisseurs, budgets, contrats et documents.
- Le module Outils permet de gérer des notes, la base de connaissance, les réservations, les flux RSS et visualiser les rapports.
- Le module Administration permet d'administrer les utilisateurs, groupes, entités, profils, règles et dictionnaires. Il permet aussi la maintenance de l'application (sauvegarde et restauration de base, vérifier si une nouvelle version est disponible).
- Le module Configuration permet d'accéder aux options de configuration générale de GLPI : notifications, collecteurs, tâches automatiques, authentification, plugins, liens externes(*source* **GLPI.org**)

# Question 5

- Upkeep(GMAO basée sur le cloud qui rationalise les ordres de travail et la gestion des installations. Créez des projets, attribuez des ordres de travail et gérez les actifs.)
- SolarWinds Service Desk(Gagnez du temps tout en proposant des services plus rapides et plus intelligents. Faites le suivi de votre matériel, vos logiciels, vos contrats et vos licences dans une solution unique et simple.)
- Freshservice(Un centre de service ITSM (Information Technology Service Management) en ligne avec gestion des incidents, des problèmes, des changements des versions et des ressources)
- Atera(Gestion et surveillance à distance (Remote Monitoring Management, RMM), automatisation des services professionnels (Professional Service Automation, PSA) et accès à distance)(*source* **capterra.be**)
