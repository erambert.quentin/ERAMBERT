<?php
    $serveur = "localhost:3306";
    $dbname = "ristoo";
    $user = "root";
    $pass = "root";
    
    $nom = $_POST["nom"];
    $prenom = $_POST["prenom"];
    $email = $_POST["email"];
    $date_de_naissance = $_POST["date_de_naissance"];
    $tel = $_POST["numero_de_telephone"];
    
    try{
        $dbco = new PDO("mysql:host=localhost:3306;dbname=ristoo",$user,$pass);
        $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
       
        $sth = $dbco->prepare("
            INSERT INTO user(nom, prenom, mail, date_de_naissance, numero_de_telephone) 
            VALUES (:nom, :prenom, :email, :date_de_naissance, :numero_de_telephone)"); 
        $sth->bindParam(':nom',$nom);
        $sth->bindParam(':prenom',$prenom);
        $sth->bindParam(':email',$email);
        $sth->bindParam(':date_de_naissance',$date_de_naissance);
        $sth->bindParam(':numero_de_telephone',$tel);
        $sth->execute();
        
        
    }
    catch(PDOException $e){
        echo 'Impossible de traiter vos données. Erreur : '.$e->getMessage();
    }
?>
