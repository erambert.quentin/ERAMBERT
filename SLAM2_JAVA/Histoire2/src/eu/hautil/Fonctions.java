package eu.hautil;

import java.sql.*;


public class Fonctions {
    String driver="com.mysql.cj.jdbc.Driver";// Aller sur mysql
    // " user=minty&password=greatsqldb " <-- Query string // Aller sur le site de mysql (voir nom complet de la classe driver)
    String urlBDD="jdbc:mysql://localhost:3306/erambqq?";
    String loginBDD="root";
    String mdpBDD="root";

    String resultAfficher = "SELECT id_role, libelle FROM who_roles";
    String resultAjouter = "INSERT INTO who_roles VALUES (?, ?)";
    String resultSupprimer = "DELETE FROM who_roles WHERE id_role=?";
    String resultAfficherRoleById = "SELECT libelle FROM who_roles WHERE id_role = ?";


    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        //System.out.println("Connexion OK");


        return conn;
    }

    public void afficherRoles()throws ClassNotFoundException, SQLException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions

        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(resultAfficher);
        System.out.println("\n" + "Vous etes dans la table : \n");
        while (result.next()) {
            System.out.println("ID : " + result.getInt(1) + "  Roles : " + result.getString(2) + " | ");

        }
    }
    public void ajouterRoles(int id, String libelle) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultAjouter);
        pstmt.setInt(1, id);
        pstmt.setString(2, libelle);
        int res= pstmt.executeUpdate();

    }
    public void supprimerRole(int id2) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultSupprimer);
        pstmt.setInt(1, id2);


        int res2= pstmt.executeUpdate(); /* executeUpdate() de java.sql.PreparedStatement
        s’exécutera avec les nouvelles valeurs ajoutées. Elle retourne le nombre de lignes modifiées par la requête. */

    }
    public void afficherRoleById(int id3) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        //Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultAfficherRoleById);
        pstmt.setInt(1, id3);

        //int res3= pstmt.executeUpdate();
        ResultSet result2 = pstmt.executeQuery();

        while (result2.next()) {
            System.out.print(" Roles : " + result2.getString(1) + "\n");
        }
        result2.close();


    }


}



