package eu.hautil.Histoire3.dao;

import eu.hautil.Histoire3.model.Role;

import java.sql.*;

public class RoleDAO {
    String driver="com.mysql.cj.jdbc.Driver";
    String urlBDD="jdbc:mysql://localhost:3306/bougur?";
    String loginBDD="root";
    String mdpBDD="root";

    String resultAfficherRoleById = "SELECT libelle,id_role FROM who_roles WHERE id_role = ?";
    String resultAjouterRole = "INSERT INTO who_roles (id_role,libelle) VALUES (?, ?)";
    String resultSupprimerRole = "DELETE FROM who_roles WHERE id_role=?";

    private Connection getConnection() throws ClassNotFoundException, SQLException {

        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        //System.out.println("Connexion OK");

        return conn;
    }

    public Role getRoleById(int id) throws SQLException, ClassNotFoundException {



        Role monRole=null;

        Connection conn = this.getConnection();

        PreparedStatement pstmt = conn.prepareStatement(resultAfficherRoleById);
        pstmt.setInt(1, id);

        ResultSet result = pstmt.executeQuery();


        if (result.next()) {
            String libelle=result.getString(1);
            int id_rec=result.getInt(2);
            monRole = new Role(libelle,id_rec);
        }
        result.close();

        return monRole;
    }
    public void ajouterRole(Role r) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection();

        PreparedStatement stmt = conn.prepareStatement(resultAjouterRole);
        stmt.setInt(1, r.getId());
        stmt.setString(2, r.getLibelle());
        int res= stmt.executeUpdate();
    }
    public void supprimerRole(int id) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection();
        PreparedStatement stmt = conn.prepareStatement(resultSupprimerRole);
        stmt.setInt(1, id);
        int res=stmt.executeUpdate();

    }

}
