package eu.hautil.histoire3.dao;

import eu.hautil.Histoire3.model.Role;
import eu.hautil.Histoire3.model.Utilisateur;

import java.sql.*;
import java.util.Date;

public class UtilisateurDAO {
    String driver="com.mysql.cj.jdbc.Driver";// Aller sur mysql
    //String urlBDD="jdbc:mysql://sio-hautil.eu:3306/bougur?"; // " user=minty&password=greatsqldb " <-- Query string // Aller sur le site de mysql (voir nom complet de la classe driver)
    String urlBDD="jdbc:mysql://localhost:3306/erambqq?";
    //String loginBDD="bougur";// login de mon serveur mysql local
    String loginBDD="root";
    //String mdpBDD="bougur";// mdp de mon serveur mysql local
    String mdpBDD="root";

    String resultAfficherUtilisateurs ="SELECT id, login, mdp, role, date_creation, nom, prenom, libelle FROM who_utilisateurs INNER JOIN who_roles on id_role=role ORDER BY id";
    String resultAfficherUtilisateursByLogin ="SELECT id, login, mdp , role, date_creation, nom, prenom, id_role, libelle FROM who_utilisateurs INNER JOIN who_roles on role = id_role WHERE login = ?";
    String resultAjouterUtilisateur ="INSERT INTO who_utilisateurs (id, login, mdp, role, nom, prenom) VALUES (?, ?, PASSWORD(?), ?, now(), ?, ?)";
    String resultSupprimerUtilisateur ="DELETE FROM who_utilisateurs WHERE id=?";

    public Connection getConnection() throws ClassNotFoundException, SQLException {

        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        //System.out.println("Connexion OK");

        return conn;
    }
    public void ajouterUtilisateur(Utilisateur u) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection();

        PreparedStatement stmt = conn.prepareStatement(resultAjouterUtilisateur);

        stmt.setInt(1,u.getId());
        stmt.setString(2,u.getLogin());
        stmt.setString(3,u.getMdp());
        stmt.setString(4,u.getNom());
        stmt.setString(5,u.getPrenom());


        int result= stmt.executeUpdate();
    }
}

