package eu.hautil.Histoire3.model;


import java.util.Date;

public class Utilisateur {

    private int id;
    private String mdp;
    private String login;
    private String nom;
    private String prenom;
    private int role;

    public Utilisateur(int id, String mdp, String login, String nom, String prenom, int role) {
        this.id = id;
        this.mdp = mdp;
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
    }

    public Utilisateur(String mdp, String login,String nom, String prenom, int role) {
        this.mdp = mdp;
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.role = role;
    }
    public int getId(){
        return id;
    }
    public String getLogin(){
        return login;
    }
    public String getMdp(){
        return mdp;
    }
    public String getNom(){
        return nom;
    }
    public String getPrenom(){
        return prenom;
    }
    public int getRole(){
        return role;
    }
}
