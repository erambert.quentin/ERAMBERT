package eu.hautil;

import java.sql.*;


public class Fonctions {
    String driver = "com.mysql.cj.jdbc.Driver";// Aller sur mysql
    // " user=minty&password=greatsqldb " <-- Query string // Aller sur le site de mysql (voir nom complet de la classe driver)
    String urlBDD = "jdbc:mysql://localhost:3306/erambqq?";
    String loginBDD = "root";
    String mdpBDD = "root";

    String resultAfficher = "SELECT id_role, libelle FROM who_roles";
    String resultAjouter = "INSERT INTO who_roles VALUES (?, ?)";
    String resultSupprimer = "DELETE FROM who_roles WHERE id_role=?";
    String resultAfficherRoleById = "SELECT libelle FROM who_roles WHERE id_role = ?";
    String resultAfficherUtilisateurs = "SELECT id, login, mdp, role, date_creation, nom, prenom, id_role, libelle FROM who_utilisateurs, who_roles WHERE role=id_role";
    String resultAfficherRoleByLogin = "SELECT id, login, mdp, role, date_creation, nom, prenom, id_role, libelle FROM who_utilisateurs, who_roles WHERE id_role=role AND login=?";

    private Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName(driver);
        Connection conn = DriverManager.getConnection(urlBDD, loginBDD, mdpBDD);
        //System.out.println("Connexion OK");


        return conn;
    }

    public void afficherRoles() throws ClassNotFoundException, SQLException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions

        Statement stmt = conn.createStatement();
        ResultSet result = stmt.executeQuery(resultAfficher);
        System.out.println("\n" + "Vous etes dans la table : \n");
        while (result.next()) {
            System.out.println("ID : " + result.getInt(1) + "  Roles : " + result.getString(2) + " | ");

        }
    }

    public void ajouterRoles(int id, String libelle) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultAjouter);
        pstmt.setInt(1, id);
        pstmt.setString(2, libelle);
        int res = pstmt.executeUpdate();

    }

    public void supprimerRole(int id2) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultSupprimer);
        pstmt.setInt(1, id2);


        int res2 = pstmt.executeUpdate(); /* executeUpdate() de java.sql.PreparedStatement
        s’exécutera avec les nouvelles valeurs ajoutées. Elle retourne le nombre de lignes modifiées par la requête. */

    }

    public void afficherRoleById(int id3) throws SQLException, ClassNotFoundException {
        Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
        //Statement stmt = conn.createStatement();
        PreparedStatement pstmt = conn.prepareStatement(resultAfficherRoleById);
        pstmt.setInt(1, id3);

        //int res3= pstmt.executeUpdate();
        ResultSet result2 = pstmt.executeQuery();

        while (result2.next()) {
            System.out.print(" Roles : " + result2.getString(1) + "\n");
        }
        result2.close();


    }

    public void afficherUtilisateurs() throws ClassNotFoundException, SQLException {
        Connection conn = this.getConnection();
        Statement stmt = conn.createStatement();
        ResultSet result4 = stmt.executeQuery(resultAfficherUtilisateurs);
        System.out.println("\n" + "Vous etes dans la table : \n");
        while (result4.next()) {
            System.out.println("id : " + result4.getInt(1)
                    + " login : " + result4.getString(2)
                    + " mdp : " + result4.getString(3)
                    + " role : " + result4.getInt(4)
                    + " date_creation : " + result4.getString(5)
                    + " nom : " + result4.getString(6)
                    + " prenom : " + result4.getString(7)
                    + " id_role : " + result4.getInt(8)
                    + " libelle: " + result4.getString(9));
        }
    }
        public void afficherRoleByLogin(String login) throws SQLException, ClassNotFoundException {
            Connection conn = this.getConnection(); //Ajouter a chaque nouvelle fonctions
            //Statement stmt = conn.createStatement();

            PreparedStatement pstmt = conn.prepareStatement(resultAfficherRoleByLogin);
            pstmt.setString(1, login);
            ResultSet result5 = pstmt.executeQuery();

            if (result5.next()) {
                System.out.println("id : " + result5.getInt(1)
                        + " login : " + result5.getString(2)
                        + " mdp : " + result5.getString(3)
                        + " role : " + result5.getInt(4)
                        + " date_creation : " + result5.getString(5)
                        + " nom : " + result5.getString(6)
                        + " prenom : " + result5.getString(7)
                        + " id_role : " + result5.getInt(8)
                        + " libelle: " + result5.getString(9));
            }
            result5.close();
            }



    }



