package eu.hautil;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        Fonctions lesFonctions = new Fonctions(); //Type Variable Constructeur


        try {
            int choix = 1;
            Scanner sc = new Scanner(System.in);


            while (choix >= 1 && choix <= 4) {
                lesFonctions.afficherRoles();
                System.out.println("\n");
                System.out.println("que voulez vous faire");
                System.out.println("\n" + "1 = Afficher la table \n" +
                        "2 = Ajouter un élément \n" +
                        "3 = Supprimer un élément\n" +
                        "4 = Afficher un élément grace a son id \n" +
                                "5 = Afficher un utilsateurs \n" +
                        "6 = afficher utilisateur par son login\n" +
                        "Autre = Quitter");
                choix = sc.nextInt();


                switch (choix) {


                    case 1:

                        lesFonctions.afficherRoles(); // on ajoute ()
                        break;


                    case 2:

                        System.out.println("Entrée la valeur de l'id a ajouter \n");
                        int id = sc.nextInt();
                        sc.nextLine();
                        System.out.println("Entrée le nom du libelle a ajouter \n");
                        String libelle = sc.nextLine();
                        lesFonctions.ajouterRoles(id, libelle);
                        break;


                    case 3:

                        System.out.println("Entrée la valeur de l'id a supprimer \n");
                        int id2 = sc.nextInt();
                        lesFonctions.supprimerRole(id2);
                        break;

                    case 4:

                        System.out.println("Entrée la valeur de l'id a afficher");
                        int id3 = sc.nextInt();
                        lesFonctions.afficherRoleById(id3);
                        break;

                    case 5:

                        lesFonctions.afficherUtilisateurs();
                        break;

                    case 6:

                        System.out.println("Entrée la valeur du login a afficher");
                        String login = sc.next();
                        lesFonctions.afficherRoleByLogin(login);
                        break;




                    default:
                        System.out.println(("au revoir"));
                        break;

                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
        // classNotFoundException : Driver n'est pas trouvé - il faut le récupérer et le lier au projet
        // Copier jar dans le projet pour faciliter les déplacement
    }

}
