package eu.hautil;


import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;


public class Main {

    public static void main(String[] args) {

        String driver="com.mysql.cj.jdbc.Driver";
        //String urlBDD="jdbc:mysql://sio-hautil.eu:3306/erambqq?"; // " user=minty&password=greatsqldb " <-- Query string // Aller sur le site de mysql (voir nom complet de la classe driver)
        String urlBDD="jdbc:mysql://localhost:3306/bougur?";
        //String loginBDD="erambqq";// login  serveur mysql local
        String loginBDD="root";
        //String mdpBDD="bougur";// mot de passe de mon serveur mysql local
        String mdpBDD="root";
        String req1 = "SELECT id_role, libelle FROM who_roles";
        String req2 = "INSERT INTO who_roles VALUES (4, 'Cuisinier')";

    try {
            Class.forName(driver);
            Connection conn= DriverManager.getConnection(urlBDD,loginBDD,mdpBDD);
            System.out.println("connection ok" + conn );
            Statement stmt = conn.createStatement();


            ResultSet result = stmt.executeQuery(req1);
            while(result.next()){
                System.out.println("Id : " + result.getInt(1));
                System.out.println("Roles : " + result.getString(2));
            }

            int res = stmt.executeUpdate(req2);

            ResultSet result2 = stmt.executeQuery(req1);

            while(result2.next()){
                System.out.println("Id : " + result2.getInt(1));
                System.out.println("Roles : " + result2.getString(2) + " lignes modifiées " + res);
            }

            int res2 = stmt.executeUpdate(req3);

            ResultSet result3 = stmt.executeQuery(req1);

            while(result3.next()){
                System.out.println("Id : " + result2.getInt(1));
                System.out.println("Roles : " + result2.getString(2) + " lignes modifiées " + res);
            }


            result2.close();
            result3.close();    
            result.close();
            stmt.close();



            conn.close(); //Obligatoire on doit fermer la connexion une fois terminer

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
  
    }
}
