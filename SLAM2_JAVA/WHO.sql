-- Adminer 4.7.7 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

INSERT INTO `who_roles` (`id_role`, `libelle`) VALUES
(1,	'hotesse acceuil'),
(2,	'secretaire'),
(3,	'agent de surface');

DROP TABLE IF EXISTS `who_utilisateurs`;
CREATE TABLE `who_utilisateurs` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `login` varchar(30) NOT NULL,
  `mdp` varchar(30) NOT NULL,
  `role` int(3) NOT NULL,
  `date_creation` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `login` (`login`),
  KEY `role` (`role`),
  CONSTRAINT `utilisateurs_ibfk_1` FOREIGN KEY (`role`) REFERENCES `who_roles` (`id_role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

INSERT INTO `who_utilisateurs` (`id`, `login`, `mdp`, `role`, `date_creation`, `nom`, `prenom`) VALUES
(1,	'erambq',	'erambq',	3,	'2021-10-06 00:00:00',	'erambert',	'quentin'),
(2,	'bougur',	'19112000',	2,	'2021-10-06 00:00:00',	'bouguetof',	'ryan'),
(3,	'mariej',	'22022002',	1,	'2021-10-06 00:00:00',	'marie louise',	'josue');

-- 2021-10-06 08:49:00
