package eu.hautil.demojavafx;

public class Role {
    private int id; //par default 0
    private String libelle; //par default null

    public int getId(){
        return this.id;
    }
    public String getLibelle(){
        return this.libelle;
    }

    public void setId(int id){
        this.id=id;
    }
    public void setLibelle(String libelle){
        this.libelle=libelle;
    }

    public Role(String l, int id){
        this.id=id;
        this.libelle=l;

    }


}
