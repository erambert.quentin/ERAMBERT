package eu.hautil.demojavafx;


import java.time.LocalDate;

public class Utilisateur {

    private int id;
    private String mdp;
    private String login;
    private String nom;
    private String prenom;
    private LocalDate date;
    private Role r;

    public Utilisateur(int id, String mdp, String login, String nom, String prenom, LocalDate date, Role r) {
        this.id = id;
        this.mdp = mdp;
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.date = date;
        this.r = r;
    }

    /*public Utilisateur(String mdp, String login, String nom, String prenom,Role r) {
        this.mdp = mdp;
        this.login = login;
        this.nom = nom;
        this.prenom = prenom;
        this.r = r;
    }*/
    public int getId(){
        return id;
    }
    public String getLogin(){
        return login;
    }
    public String getMdp(){
        return mdp;
    }
    public String getNom(){
        return nom;
    }
    public String getPrenom(){
        return prenom;
    }
    public LocalDate getDate() {return date;}
    public Role Role() {return r ;}
    }



