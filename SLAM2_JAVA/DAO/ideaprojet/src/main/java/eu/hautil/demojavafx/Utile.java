package eu.hautil.demojavafx;


import java.time.LocalDate;
import java.util.ArrayList;

public class Utile {
    private ArrayList<Utilisateur> utilisateurs;

    public Utile() {
        this.utilisateurs = new ArrayList<>();
        //creation de 5 utilisateurs a ajouter dans l'ArrayList
        for (int i = 1; i < 6; i++) {
            Role r = new Role("role"+i,i);

            Utilisateur u = new Utilisateur(i, "mdp" + i, "login" + i, "nom" + i, "prenom" + i, LocalDate.now(), r);
            this.utilisateurs.add(u);
        }
    }

    public boolean estUtilisateurConnu(String login, String mdp) {

        boolean existe = false;
        //Cette méthode :
        // 1. doit parcourir la liste à sa disposition
        // pour vérifier l'existence de l'utilisateur passé en paramètre
        // 2. Si l'utlisateur existe avec le bon mot de passe, on retourne VRAI
        // 3. Sinon on retourne FAUX
        //equals signifie
        for (Utilisateur u : this.utilisateurs) {
            if (login.equals(u.getLogin())&& mdp.equals(u.getMdp())) ; //u.getLogin
            {
                existe = true;
                break;
            }

        }

        return existe;
    }
}
/*methode main pour tester notre classe utile
        public static void main (String[]args){
        Utile u= new Utile();
        boolean res= u.estUtilisateurConnu("login1","mdp2");
        if(res) System.out.println("youpi vous existez");
        else System.out.println
        }*/