package eu.hautil.demojavafx;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

// si je veux créer une application (fenêtre), il faut que ma classe hérite  de la classe javafx.application.Application
public class HelloApplication extends Application {
    // start me permet d'initialiser les éléments graphiques et la gestion des events
    @Override
    public void start(Stage primaryStage) throws IOException {
        /* bloc proposé par intellij à l'initialisation
        // dans une application , on a un stage, qui contient une scène, qui contient un root panel
        //FXML Loader sert à charger le canva d'une fenêtre (fxml correspondant)
        FXMLLoader fxmlLoader = new FXMLLoader(HelloApplication.class.getResource("hello-view.fxml"));
        //scene est l'objet qui permet de donner des dimensions à la fenêtre, en fixant le décor principal (root panel)
        Scene scene = new Scene(fxmlLoader.load(), 500, 500);
        //stage correspond à la fenêtre principale lors du démarrage de l'application
        stage.setTitle("Hello!"); // on change le titre de la fenêtre
        stage.setScene(scene);
        //instruction essentielle pour voir la fenêtre
        stage.show();
         */
        //code proposé par le tuto oracle
        primaryStage.setTitle("JavaFX Welcome");
        // Gridpane est une sorte de panel en forme de tableau
        GridPane grid = new GridPane();
        //pour centrer les éléments dans les cellules
        grid.setAlignment(Pos.CENTER);
        //gap entre le bout de cellule et l'élément graphique dans la cellule
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        // astuce de deboggage
        grid.setGridLinesVisible(true);
        // création des objets graphiques à poser sur le panel grid
        Text scenetitle = new Text("Welcome");
        scenetitle.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
        // ajout de l'objet graphique dans le panel dans une position donnée
        grid.add(scenetitle, 0, 0, 2, 1);

        Label userName = new Label("User Name:");
        grid.add(userName, 0, 1);

        TextField userTextField = new TextField();
        grid.add(userTextField, 1, 1);

        Label pw = new Label("Password:");
        grid.add(pw, 0, 2);

        PasswordField pwBox = new PasswordField();
        grid.add(pwBox, 1, 2);

        //ajout du bouton
        Button btn = new Button("Sign in");
        // HBox est un panel horizontal, qui accueille les objets graphiques dans l'ordre de gauche à droite
        HBox hbBtn = new HBox(10);
        hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
        //ajout du bouton dans hbox créé
        hbBtn.getChildren().add(btn);
        //hbox est ajouté à grid à une position donnée
        grid.add(hbBtn, 1, 4);
        // ajout du composant Text pour afficher les messages personnalisés
        // actionTarget (grâce à final) sera accessible partout dans le code de cette classe
        final Text actiontarget = new Text();
        grid.add(actiontarget, 1, 6); //2e colonne, 7e ligne
        // partie gestion d'event sur le bouton
        btn.setOnAction(new EventHandler<ActionEvent>() {

            //redéfinition de la seule méthode de l'interface EventHandler
            @Override
            public void handle(ActionEvent e) {
                // 1 . récupérer le login saisi et grâce trim() enlever les espaces (caractères cachés) avant et après
                String login = userTextField.getText().trim();
                // 2. récupérer le mot de passe saisi
                String pwd = pwBox.getText();

                Utile u = new Utile();
                boolean res = u.estUtilisateurConnu(login, pwd);
                if (res) {
                    System.out.println("bravo");
                } else {
                    System.out.println("ca ne vas pas etre possible");
                }
            }
                // 3 . écrire message particulier si un ou les deux sont vides
                // tester si login est vide
                /*if(login.equals(""))
                {
                    //manière 1 : log system
                    System.out.println("login vide");
                    //manière 2 : message graphique explicite
                    actiontarget.setFill(Color.FIREBRICK);
                    actiontarget.setText("Remplir le login SVP");
                }else{
                    actiontarget.setFill(Color.FORESTGREEN);
                    actiontarget.setText("Bien joué "+ login);
                }
                // 4 . si les deux ne sont pas vide, vérifier si on a le bon login et mot de passe ("admin", "adminFort")

                //actiontarget.setText("Sign in button pressed");
            }*/
        });

                // création de la scene en y ajoutant l'objet grid comme root panel et on a définit la taille de la fenêtre
                Scene scene = new Scene(grid, 300, 275);
                // ajouter la scene à la fenêtre (primary stage)
                primaryStage.setScene(scene);
                primaryStage.show();

            }

            ;

            public static void main(String[] args) {
                //launch est une méthode public et statique appelé pour démarrer l'application en utilisant start
                launch();
            }
        }
