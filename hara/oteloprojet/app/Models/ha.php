<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ha extends Model
{
    use HasFactory;
    protected $table ="chevaux";
    protected $fillable = [
        'nom' , 'age' , 'couleur'
    ];
}