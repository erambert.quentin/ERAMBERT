package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class HotelDescriptionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hotel_description)
        val TV1=findViewById<TextView>(R.id.activity_hotel_description_textview)

        val action = intent.action  //on ne s en sert pas ici
        val isHotel = intent.hasCategory("Hotel") //on ne s en sert pas ici
        val name = intent.getStringExtra("R'BNB")
        TV1.setText(name)

    }


}