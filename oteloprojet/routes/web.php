<?php
use App\Http\Controllers\PremierController;
use App\Http\Controllers\ChambreController;
use App\Http\Controllers\reservationController;
use App\Http\Controllers\haraController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/acceuil', function () {
    return view('acceuil');
});
Route::get('/hara',[haraController::class, 'hara'] );
Route::get('/accewuil',[PremierController::class, 'home'] );
Route::get('/chambre',[ChambreController::class, 'store'] );
Route::get('/newreservation',[reservationController::class, 'create'] )->middleware('auth');
Route::get('/failure',function () {
    return view('failure');
})->name('failure');
Route::post('/storereservation',[ReservationController::class, 'store'] )->name('reservation.store');
Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/deconnexion',function () {
    Auth::logout();
     return redirect('/');
});
Route::get('/chambres',[ChambreController::class, 'index'] );


