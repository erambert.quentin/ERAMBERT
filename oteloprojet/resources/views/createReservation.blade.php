@extends('layouts.app')

@section('content')
<div class="container">
@if($errors->any())
 <div class='alert alert-danger'>
   <ul>
    @foreach($errors->all() as $error)
        <li>{{$error}}</li>
    @endforeach    
   </ul>
@endif

 <script>
  var today = new Date();
  var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
  document.getElementById("currentDate").value = new Date();
</script>
<div class="container">
<form method="post" action="{{ route('reservation.store')}}">
@csrf
  <div class="form-group">
    <label for="exampleInputEmail1">date debut</label>
    <input name="dated" type="date" class="form-control @error('dated') is-invalid @enderror" id="exampleInputEmail1"  >
        @error('dateD')
        <div class="alert alert-danger mt-2">
        {{$message}} mon message perso
        </div>
        @enderror
     </div>
  <div class="form-group">
    <label for="exampleInputPassword1">date fin</label>
    <input name="datef" type="date" class="form-control" id="exampleInputPassword1" >
    
  </div>
  <div class="form-group">
  <label for="sel1">Select list:</label>
  <select name="idperiode" class="form-control" id="sel1">
    <option value=1>basse</option>
    <option value=2>moyenne</option>
    <option value=3>haute</option>
  </select>
  </div>
  <div class="col-2">
  <label for="sel2">Catégorie</label>
  <select name="idcategorie" class="form-control" id="sel2">
    <option value=1>standard</option>
    <option value=2>confort</option>
    <option value=3>premium</option>
    <option value=4>luxe</option>
  </select>
  </div>
</div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>

@stop