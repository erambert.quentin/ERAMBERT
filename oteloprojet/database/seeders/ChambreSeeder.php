<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Chambre;

class ChambreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($a = 0; $a <= 29; $a++) {

            $input = array('A', 'B', 'C');
            $i = rand(0, 2);
            $chambre = Chambre::create([
                'nbCouchage' => rand(1, 3),
                'porte' => $input[$i],
                'etage' => rand(1, 12),
                'idCategorie' => rand(1, 4),
                'baignoire' => rand(0, 2),
                'prixBase' => rand(25, 200),
            ]);
        }
            //affichage en console
            dd($chambre);   //

    }
}