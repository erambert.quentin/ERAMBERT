# Quentin Erambert

# Les obligations de notifications en cas de faille de securité.

# 1)Les differentes types de failles de securités.

Les 5 failles de sécurité informatiques en entreprise les plus répandues.

- 1 La messagerie d’entreprise
- 2 La gestion des accès
- 3 Les logiciels obsolètes
- 4 Le télétravail et l’utilisation de matériel personnel
- 5 L’hébergement des données

 # 2)Le processus de notification prevu par le RGPD

 - Si l’incident constitue un risque au regard de la vie privée des personnes concernées, vous devrez notifier l’incident à la CNIL.
En cas de risque élevé, vous devez également notifier les personnes concernées.
En cas de doute, notifiez à la CNIL qui vous indiquera s’il est nécessaire d’informer les personnes.
L'obligation de notifier à la CNIL les violations de données à caractère personnel est prévue à l’article 33 du règlement général sur la protection des données (RGPD). Elle concerne tous les responsables de traitement de données à caractère personnel. Dans le cas où la violation de données à caractère personnel est susceptible d’engendrer un risque élevé pour les droits et libertés d’une personne physique, l’article 34 du RGPD impose de notifier ces dernières.
- sanction max = 20 millions d'euros

# 3)La Politique interne concernant les failles de securité.

- Le RGPD (articles 33 et 34) a instauré une obligation de notification, obligeant les responsables de traitement à notifier toute violation de données personnelles à l’autorité de contrôle compétente, et ce dans un délai de 72 heures à compter de sa découverte.

- Enfin, la preuve du respect des obligations liées à la sécurité des traitements impliquera notamment la mise en place d’un cahier des incidents (distinct du registre des traitements) comprenant l’ensemble de la documentation relative à ces incidents. Cette documentation doit comprendre la nature de la violation, les catégories et le nombre approximatif des personnes concernées par la violation et des enregistrements de données personnes concernés, les conséquences probables de la violation de données, et les mesures prises ou envisagées pour atténuer les éventuelles conséquences négatives ou pour éviter que l’incident se reproduise.

# 4)2 exemples reels de faille de securité.

   - Le piratage du PlayStation Network est une intrusion extérieure survenue entre le 17 et le 19 avril 2011, ayant entraîné une mise hors-ligne sans précédent puis une suspension à l'échelle mondiale de tous les services du PlayStation Network et de Qriocity
   - Le 24 janvier 2013, la société britannique Information Commissioner's Office (ICO) condamne la société européenne de Sony à une amende de 250 000 livres, soit environ 300 000 euros, reprochant à la compagnie « d'avoir négligé des failles de sécurité qui auraient pu être corrigées bien avant les fuites », ce que l'équipe de Sony avait d'ailleurs admis.


   - Considérée comme la plus importante cyberattaque par ransomware de l’histoire, WannaCry a infecté en quelques heures plus de 300 000 ordinateurs, dans plus de 150 pays. Parmi ses victimes : FedEx, Renault, ou encore le ministère de l’intérieur russe. Cette attaque a été revendiquée par le groupe de hackers Shadow Brokers. Celui-ci avait déjà sévi au premier trimestre 2017, en réussissant à s’introduire dans le réseau informatique de la NSA, et à y dérober un attirail considérable de failles, virus et autres outils informatiques, dont la faille exploitée par WannaCry, baptisée Eternal Blue. Il s’agissait d’une faille déjà identifiée par Microsoft, mais le patch correctif proposé n’avait pas été suffisamment massivement installé pour que l’attaque échoue. In fine, les coûts de WannaCry ont été évalués autour d’un milliard de dollars, sans compter bien évidemment toutes les conséquences indirectes qu’ont pu subir ses victimes.
