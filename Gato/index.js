var port = 3000;
var hostname = 'localhost'
var express = require('express');
var http = require('http');
var app = express(); 
var server = http.createServer(app);
var io = require('socket.io')(server);



app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });
  socket.on('chat message', function(msg){
    console.log('message: ' + msg);    
    io.emit('chat message', msg);
   });
});
server.listen(port,hostname, function(){ 
  console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port); 
});