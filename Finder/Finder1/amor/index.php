<?php 
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App;
$app->get('/bonjour', function(Request $request, Response $response){  
  return "bonjour";
});
$app->get('/chambre/{idc}', function(Request $request, Response $response){
  $idc = $request->getAttribute('idc');
  // $couchage = $request->getAttribute('nbCouchage');
  // $porte = $request->getAttribute('porte');
  // $etage = $request->getAttribute('etage');
  // $categorie = $request->getAttribute('idCategorie');
  // $baignoire = $request->getAttribute('baignoire');
       return getPersonnage($idc);
});
function connexion()
{
   return $dbh = new PDO("mysql:host=localhost;dbname=erambqq", 'root', 'root', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
}
function getPersonnage($idc)
{
  $sql = "SELECT * FROM categorie WHERE id=:idc";
  try{
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->bindParam(":idc", $idc, PDO::PARAM_STR);
    $statement->execute();
     $result = $statement->fetchAll(PDO::FETCH_CLASS); 
                 return json_encode($result, JSON_PRETTY_PRINT);
  } catch(PDOException $e){
    return '{"error":'.$e->getMessage().'}';
  }
}
$app->run();